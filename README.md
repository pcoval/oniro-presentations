# ONIRO PRESENTATIONS #

SPDX-License-Identifier: CC-BY-4.0

SPDX-License-URL: https://spdx.org/licenses/CC-BY-4.0.html

SPDX-FileCopyrightText: Huawei Inc.


## USAGE ##

```sh
cp -rfa docs/_template docs/TODO-$USER
docker-compose up --build
x-www-browser http://localhost:8888/
```

## RESOURCES ##

- <https://booting.oniroproject.org/rzr/oniro-presentations>
- <https://oniroproject.org/>
- <https://github.com/yjwen/org-reveal>
